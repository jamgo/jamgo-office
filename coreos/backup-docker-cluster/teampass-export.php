<?php

// The secret key
$salt = '{{teampass_saltkey}}';

// Hostname of the database server
$dbHost = 'teampass-db';

// Username for the database server
$dbUser = 'teampass';

// Password for the database server
$dbPass = '{{teampass_db_password}}';

// Name of the database
$dbName = 'teampass';

// Path and name to the exported csv
// This should not be readable for public,
// but must be writeable for the webserver user
$csvfile = './backups/teampass-backup.csv';

$_SESSION['CPM'] = 1; // Don't change this (needed before includes)

// Path to the include files - only change if this script is not in the root dir of teampass
require_once './includes/config/settings.php';
require_once './sources/main.functions.php';

$dbc = new mysqli( $dbHost, $dbUser, $dbPass, $dbName );
if( $dbc->connect_errno )
{
    die( $dbc->connect_error );
}

$sql = '
    SELECT
         i.id 
        ,i.label
        ,i.description
        ,i.pw
        ,i.url
        ,i.login
        ,i.email
        ,t.title AS foldername
    FROM teampass_items AS i
        LEFT JOIN teampass_nested_tree AS t
            ON i.id_tree = t.id
    ORDER BY i.label ASC';

$res = $dbc->query( $sql );
if( $dbc->errno )
{
    die( $dbc->error );
}

if( $res->num_rows === 0 )
{
    echo 'No Entries found.';
}
else
{
    $csvFilePointer = fopen( $csvfile, 'w' );
    if( $csvFilePointer === false )
    {
        die( 'Opening output file failed.' );
    }

//     $writeResult = fputcsv( $csvFilePointer, array('label','description','folder','email','link','login','password') );
//     if( $writeResult === false )
//     {
//         die( 'writing failed.' );
//     }

    while( $row = $res->fetch_object() )
    {
        $id = $row->id;
        $encrytedPassword = $row->pw;
        $label = $row->label;
        $description = $row->description;
        $url = $row->url;
        $login = $row->login;
        $email = $row->email;
        $foldername = $row->foldername;

        $cryptResult = cryption( $encrytedPassword, $salt, "decrypt" );
        if( is_array($cryptResult) && isset($cryptResult['string']) )
        {
            $decryptedPassword = $cryptResult['string'];
        }
        else
        {
            echo "Decryption of entry ' . $id . ' failed.\n";
            continue;
        }

        $writeResult = fputcsv( $csvFilePointer, array(
             $label
        	,"default"
        	,"default"
        	,$url
        	,$login
        	,$decryptedPassword
        	,$description
        ), ";");
        if( $writeResult === false )
        {
            die( 'writing failed.' );
        }
    }
}
?>