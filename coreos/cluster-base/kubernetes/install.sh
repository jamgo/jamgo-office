#!/bin/bash

HARDWARE_TYPE=""
SERVICE=""

function usage() {
    printf "\n"
    printf "Installs kubernetes cluster\n"
    printf "\n"
    printf "Usage:\n"
    printf "\tinstall.sh (-t|--type) (bare-metal|qemu) [(-s|--service) <service>]\n"
    printf "\tinstall.sh -h|--help\n"
    printf "\n"
    printf "Options:\n"
    printf "\tbare-metal, qemu\tHardware type.\n"
    printf "\n"
}

while [ "$1" != "" ]; do
    case $1 in
        -h | --help)
            usage
            exit
            ;;
        -s | --service)
        	if [ -n "$SERVICE" ]; then
        		printf "ERROR: Only one service per install!\n"
        		usage
        		exit 1
        	fi
        	shift
            SERVICE=$1
            ;;
        -t | --type)
        	if [ -n "$HARDWARE_TYPE" ]; then
        		printf "ERROR: Only one hardware type!\n"
        		usage
        		exit 1
        	fi
        	shift
        	case $1 in
        		bare-metal | qemu)
            		HARDWARE_TYPE=$1
            		;;
            	*)
        			printf "ERROR: Unknown hardware type\n"
        			printf "$1"
        			exit 1
        			;;
        	esac
            ;;
        *)
            printf "ERROR: unknown parameter \"$1\"\n"
            usage
            exit 1
            ;;
    esac
    shift
done

if [ -z $HARDWARE_TYPE ]; then
	printf "ERROR: Must select a hardware type.\n"
	usage
	exit 1
fi

if [ -z $SERVICE ]; then
	ansible-playbook -i ../../ansible-common/inventories/$HARDWARE_TYPE/ all.yml
else
	ansible-playbook -i ../../ansible-common/inventories/$HARDWARE_TYPE/ $SERVICE.yml
fi
