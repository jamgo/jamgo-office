#/bin/bash

set -e

cd

if [[ -e $HOME/.bootstrapped ]]; then
  exit 0
fi

PYPY_VERSION=2.7-v7.3.9

mkdir -p /opt/bin
cd /opt/bin
wget -O - https://downloads.python.org/pypy/pypy$PYPY_VERSION-linux64.tar.bz2 |tar -xjf -

mv -n pypy$PYPY_VERSION-linux64 pypy

[ -f /lib64/libncurses.so.5.9 ] && ln -snf /lib64/libncurses.so.5.9 /opt/bin/pypy/bin/libtinfo.so.5
[ -f /lib64/libncurses.so.6.1 ] && ln -snf /lib64/libncurses.so.6.1 /opt/bin/pypy/bin/libtinfo.so.5
ln -snf /opt/bin/pypy/bin/pypy /opt/bin/python

/opt/bin/python -m ensurepip
/opt/bin/pypy/bin/pip install -U pip wheel
ln -snf /opt/bin/pypy/bin/pip /opt/bin/pip

touch $HOME/.bootstrapped
