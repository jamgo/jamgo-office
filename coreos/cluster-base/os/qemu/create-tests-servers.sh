#!/bin/bash

source ./env

if [ ! -f "${IMAGES_PATH}/coreos_production_qemu_image.img" ]; then
#	wget https://stable.release.core-os.net/amd64-usr/current/coreos_production_qemu.sh
#	chmod +x coreos_production_qemu.sh
	wget https://stable.release.core-os.net/amd64-usr/current/coreos_production_qemu_image.img.bz2 -O "${IMAGES_PATH}"/coreos_production_qemu_image.img.bz2
	echo "Decompressing CoreOS image file..."
	bzip2 -d "${IMAGES_PATH}"/coreos_production_qemu_image.img.bz2
	
fi

./create-coreos-vm.sh -h wilma -m 16384 -i 10.19.0.2/16 -g 10.19.0.1 -a 52:54:00:fe:b3:c0

./create-coreos-vm.sh -h betty -m 32768 -i 10.19.0.3/16 -g 10.19.0.1 -a 52:54:00:fe:b3:c1

# https://kubernetes.io/docs/setup/independent/install-kubeadm/
# http://alesnosek.com/blog/2017/02/14/accessing-kubernetes-pods-from-outside-of-the-cluster/
# https://medium.com/@kyralak/accessing-kubernetes-services-without-ingress-nodeport-or-loadbalancer-de6061b42d72
# https://blog.laputa.io/kubernetes-flannel-networking-6a1cb1f8ec7c
# https://github.com/kubernetes/ingress-nginx
# https://coredns.io/2017/05/08/custom-dns-entries-for-kubernetes/
# https://medium.com/@geraldcroes/kubernetes-traefik-101-when-simplicity-matters-957eeede2cf8
# https://medium.com/localz-engineering/kubernetes-traefik-locally-with-a-wildcard-certificate-e15219e5255d

# sudo su
# CNI_VERSION="v0.7.4"
# mkdir -p /opt/cni/bin
# curl -L "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-linux-amd64-${CNI_VERSION}.tgz" | tar -C /opt/cni/bin -xz
# CRICTL_VERSION="v1.13.0"
# mkdir -p /opt/bin
# curl -L "https://github.com/kubernetes-incubator/cri-tools/releases/download/${CRICTL_VERSION}/crictl-${CRICTL_VERSION}-linux-amd64.tar.gz" | tar -C /opt/bin -xz
# RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
# mkdir -p /opt/bin
# cd /opt/bin
# curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}
# chmod +x {kubeadm,kubelet,kubectl}
# curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/kubelet.service" | sed "s:/usr/bin:/opt/bin:g" > /etc/systemd/system/kubelet.service
# mkdir -p /etc/systemd/system/kubelet.service.d
# curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/10-kubeadm.conf" | sed "s:/usr/bin:/opt/bin:g" > /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
# systemctl enable --now kubelet
# systemctl enable docker.service
# exit

# sudo kubeadm init --pod-network-cidr=10.244.0.0/16
# mkdir -p $HOME/.kube
# sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
# sudo chown $(id -u):$(id -g) $HOME/.kube/config
# sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

# sudo ip route add 10.29.0.0/16 dev virbr0 via 10.19.0.2 proto static

# ... Dashboard

# kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml

# dashboard-adminuser.yml

# apiVersion: v1
# kind: ServiceAccount
# metadata:
#   name: admin-user
#   namespace: kube-system
# ---
# apiVersion: rbac.authorization.k8s.io/v1
# kind: ClusterRoleBinding
# metadata:
#   name: admin-user
# roleRef:
#   apiGroup: rbac.authorization.k8s.io
#   kind: ClusterRole
#   name: cluster-admin
# subjects:
# - kind: ServiceAccount
#   name: admin-user
#   namespace: kube-system

# kubectl apply -f dashboard-adminuser.yml

# kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')


# https://github.com/giantswarm/prometheus


# kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-rbac.yaml
# kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-ds.yaml

#traefik-ui.yaml
#---
#apiVersion: v1
#kind: Service
#metadata:
#  name: traefik-web-ui
#  namespace: kube-system
#spec:
#  selector:
#    k8s-app: traefik-ingress-lb
#  ports:
#  - name: web
#    port: 80
#    targetPort: 8080
#---
#apiVersion: extensions/v1beta1
#kind: Ingress
#metadata:
#  name: traefik-web-ui
#  namespace: kube-system
#spec:
#  rules:
#  - host: traefik-ui.montoto.org
#    http:
#      paths:
#      - path: /
#        backend:
#          serviceName: traefik-web-ui
#          servicePort: web
