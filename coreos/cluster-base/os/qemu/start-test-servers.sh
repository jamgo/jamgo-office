#!/bin/bash

ssh-keygen -f ~/.ssh/known_hosts -R "10.19.0.2"
ssh-keygen -f ~/.ssh/known_hosts -R "10.19.0.3"

echo "Starting..."
virsh start wilma
virsh start betty

while ! nc -z 10.19.0.2 22; do   
	sleep 1
done
while ! nc -z 10.19.0.3 22; do   
	sleep 1
done

ssh-keyscan -H 10.19.0.2 >> ~/.ssh/known_hosts
ssh-keyscan -H 10.19.0.3 >> ~/.ssh/known_hosts