#!/bin/bash

source ./env

purge() {
	echo "Purging..."
	virsh shutdown wilma
	virsh shutdown betty
	virsh undefine wilma
	virsh undefine betty
	sudo rm wilma*
	sudo rm betty*
	sudo rm "${IMAGES_PATH}"/wilma*
	sudo rm "${IMAGES_PATH}"/betty*
	exit
}

while true
do
	read -r -p "Purge will remove also disk images. Are you Sure? [yes/no] " input

	case $input in
		[yY][eE][sS])
		purge
		;;

		[nN][oO])
		echo "Bye"
		exit
		;;

		*)
		echo "Pleas enter 'yes' or 'no'"
		;;
	esac
done