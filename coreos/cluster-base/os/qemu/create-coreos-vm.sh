#!/bin/bash

while [ $# -ge 1 ]; do
    case "$1" in
        -h|--hostname)
            HOSTNAME="$2"
            shift 2 ;;
        -a|--mac-address)
            MAC_ADDRESS="$2"
            shift 2 ;;
        -m|--memory)
            MEMORY="$2"
            shift 2 ;;
        -i|--ip)
            IP="$2"
            shift 2 ;;
        -g|--gateway)
            GATEWAY="$2"
            shift 2 ;;
        --)
            shift
            break ;;
        *)
            break ;;
    esac
done

source ./env

qemu-img create -f qcow2 -b "${IMAGES_PATH}"/coreos_production_qemu_image.img "${IMAGE_FILE}" 200G

cp ./coreos-config-template.yml "${CONFIG_FILE}"
sed -i "s|{hostname}|${HOSTNAME}|" "${CONFIG_FILE}"
sed -i "s|{ssh_key}|${SSH_KEY}|" "${CONFIG_FILE}"
sed -i "s|{mac_address}|${MAC_ADDRESS}|" "${CONFIG_FILE}"
sed -i "s|{ip}|${IP}|" "${CONFIG_FILE}"
sed -i "s|{gateway}|${GATEWAY}|" "${CONFIG_FILE}"
sudo ./ct -pretty -in-file "${CONFIG_FILE}" -out-file "${IGNITION_FILE}"

virt-install \
 --import \
 --name "${HOSTNAME}" \
 --network network=nat,model=virtio,mac="${MAC_ADDRESS}" \
 --ram "${MEMORY}" \
 --vcpus 2 \
 --os-type=linux \
 --os-variant=virtio26 \
 --disk path="${IMAGE_FILE}",format=qcow2,bus=virtio \
 --graphics none \
 --noautoconsole \
 --sound none \
 --rng /dev/urandom \
 --noreboot \
 --print-xml > "${DOMAIN}"

sed -ie 's|type="kvm"|type="kvm" xmlns:qemu="http://libvirt.org/schemas/domain/qemu/1.0"|' "${DOMAIN}"
sed -i "/<\/devices>/a <qemu:commandline>\n  <qemu:arg value='-fw_cfg'/>\n  <qemu:arg value='name=opt/com.coreos/config,file=${IGNITION_FILE}'/>\n</qemu:commandline>" "${DOMAIN}"

virsh define "${DOMAIN}"
