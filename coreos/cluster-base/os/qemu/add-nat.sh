#!/bin/sh

# ... Get interfaces & ips.

DEFAULT_INTERFACE=$(ip route | awk '/default/ { print $5 }' | head -1)
DEFAULT_IP=$(ip -4 addr show $DEFAULT_INTERFACE | awk '/inet / { print $2 }' | cut -d/ -f1)
QEMU_INTERFACE=$(ip route | awk '/10\.19\.0\.1/ { print $3 }' | head -1)

# ... Delete previous rules to avoid duplicates.

sudo iptables -t nat -D PREROUTING -i $DEFAULT_INTERFACE -p tcp --dport 80 -j DNAT --to-destination 10.19.0.2 > /dev/null 2>&1
sudo iptables -t nat -D POSTROUTING -o $QEMU_INTERFACE -p tcp --dport 80 -d 10.19.0.2 -j SNAT --to-source $DEFAULT_IP > /dev/null 2>&1
sudo iptables -t nat -D PREROUTING -i $DEFAULT_INTERFACE -p tcp --dport 443 -j DNAT --to-destination 10.19.0.2 > /dev/null 2>&1
sudo iptables -t nat -D POSTROUTING -o $QEMU_INTERFACE -p tcp --dport 443 -d 10.19.0.2 -j SNAT --to-source $DEFAULT_IP > /dev/null 2>&1

# ... Add nat rules to route incomming connection on port 80 and 443, to kubernetes master node.

sudo iptables -t nat -A PREROUTING -i $DEFAULT_INTERFACE -p tcp --dport 80 -j DNAT --to-destination 10.19.0.2
sudo iptables -t nat -A POSTROUTING -o $QEMU_INTERFACE -p tcp --dport 80 -d 10.19.0.2 -j SNAT --to-source $DEFAULT_IP
sudo iptables -t nat -A PREROUTING -i $DEFAULT_INTERFACE -p tcp --dport 443 -j DNAT --to-destination 10.19.0.2
sudo iptables -t nat -A POSTROUTING -o $QEMU_INTERFACE -p tcp --dport 443 -d 10.19.0.2 -j SNAT --to-source $DEFAULT_IP

# ... Delete rules that block access to the qemu interface.

sudo iptables -D FORWARD -o $QEMU_INTERFACE -j REJECT > /dev/null 2>&1
sudo iptables -D FORWARD -i $QEMU_INTERFACE -j REJECT > /dev/null 2>&1

# ... Add routes to pods and services subnets.

sudo ip route add 10.47.0.0/16 via 10.19.0.2
sudo ip route add 10.53.0.0/16 via 10.19.0.2