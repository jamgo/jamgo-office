#!/bin/bash

while [ $# -ge 1 ]; do
    case "$1" in
        -h|--hostname)
            HOSTNAME="$2"
            shift 2 ;;
        -a|--mac-address)
            MAC_ADDRESS="$2"
            shift 2 ;;
        -i|--ip)
            IP="$2"
            shift 2 ;;
        -g|--gateway)
            GATEWAY="$2"
            shift 2 ;;
        --)
            shift
            break ;;
        *)
            break ;;
    esac
done

mkdir ./build

IGNITION_FILE=./build/"${HOSTNAME}".ign
CONFIG_FILE=./build/"${HOSTNAME}".yml
SSH_KEY=$(cat ~/.ssh/id_rsa.pub)

cp ./coreos-config-template.yml "${CONFIG_FILE}"
sed -i "s|{hostname}|${HOSTNAME}|" "${CONFIG_FILE}"
sed -i "s|{ssh_key}|${SSH_KEY}|" "${CONFIG_FILE}"
sed -i "s|{mac_address}|${MAC_ADDRESS}|" "${CONFIG_FILE}"
sed -i "s|{ip}|${IP}|" "${CONFIG_FILE}"
sed -i "s|{gateway}|${GATEWAY}|" "${CONFIG_FILE}"
sudo ./ct -pretty -in-file "${CONFIG_FILE}" -out-file "${IGNITION_FILE}"
