#!/bin/bash

./create-ignition.sh -h wilma -i 10.7.13.10/24 -g 10.7.13.1 -a 54:04:a6:7f:40:72

./create-ignition.sh -h betty -i 10.7.13.11/24 -g 10.7.13.1 -a 2c:4d:54:45:04:f1

# wget https://raw.github.com/coreos/init/master/bin/coreos-install
# coreos-install -v -d /dev/sda -C stable -i [wilma|betty].ign

# TODO: agregar el timezone en los *.ign
# sudo timedatectl set-timezone Europe/Andorra