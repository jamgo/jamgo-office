#!/bin/bash

HARDWARE_TYPE=""
SERVICE=""
EXTRA_OPTIONS=""
UPDATE_DCODE=""
UPDATE_DATA=""
EXTRA_VARS=""

function usage() {
    printf "\n"
    printf "Installs kubernetes cluster\n"
    printf "\n"
    printf "Usage:\n"
    printf "\tinstall.sh (-t|--type) (bare-metal|qemu) [--update-code] [--update-data] [(-o|--options) <options>] <service>\n"
    printf "\tinstall.sh -h|--help\n"
    printf "\n"
    printf "Options:\n"
    printf "\tbare-metal, qemu\tHardware type.\n"
    printf "\n"
}

while [ "$1" != "" ]; do
    case $1 in
        -h | --help)
            usage
            exit
            ;;
        -t | --type)
        	if [ -n "$HARDWARE_TYPE" ]; then
        		printf "ERROR: Only one hardware type!\n"
        		usage
        		exit 1
        	fi
        	shift
        	case $1 in
        		bare-metal | qemu)
            		HARDWARE_TYPE=$1
            		;;
            	*)
        			printf "ERROR: Unknown hardware type\n"
        			printf "$1"
        			exit 1
        			;;
        	esac
        	;;
        -o | --options)
        	shift
            EXTRA_OPTIONS=$1
            ;;
        --update-code)
            UPDATE_CODE="true"
            ;;
        --update-data)
            UPDATE_DATA="true"
            ;;
        *)
        	if [ -n "$SERVICE" ]; then
        		printf "ERROR: Only one service at a time!\n"
            usage
            exit 1
        	fi
            SERVICE=$1
            ;;
    esac
    shift
done

if [ -z "$HARDWARE_TYPE" ]; then
	printf "ERROR: Must select a hardware type.\n"
	usage
	exit 1
fi

if [ -z "$SERVICE" ]; then
	printf "ERROR: Must select a service.\n"
	usage
	exit 1
fi

if [ "$UPDATE_CODE" = "true" ]; then
	EXTRA_VARS="$EXTRA_VARS update_code=true"
fi

if [ "$UPDATE_DATA" = "true" ]; then
	EXTRA_VARS="$EXTRA_VARS update_data=true"
fi

if [ ! -z "$EXTRA_VARS" ]; then
	EXTRA_VARS="--extra-vars \"$EXTRA_VARS\""
fi

ansible-playbook -i ../ansible-common/inventories/$HARDWARE_TYPE/ $SERVICE.yml $EXTRA_VARS $EXTRA_OPTIONS
