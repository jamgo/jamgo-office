---
kind: ConfigMap
apiVersion: v1
metadata:
  name: josepcarol-nginx-conf
  namespace: {{josepcarol_kube_ns}}
data:
  default.conf: |
    server {
      listen 80;
      server_name {{josepcarol_domain}};
      root /var/www/html;
    
      rewrite_log on;
      
      index index.php; 
    
      location / {
        try_files $uri $uri/ /index.php?$args;
      }
    
      rewrite /wp-admin$ $scheme://$host$uri/ permanent;
    
      rewrite /map/embed$ $scheme://$host/wp-content/themes/JointsWP-master/map.php;
      
      location ~ [^/]\.php(/|$) { 
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        if (!-f $document_root$fastcgi_script_name) {
          return 404;
        }
    
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO       $fastcgi_path_info;
        fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
    
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
      } 
    }
---
kind: PersistentVolume
apiVersion: v1
metadata:
  name: "{{josepcarol_web_volume}}-pv"
spec:
  storageClassName: local-storage
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  local:
    path: {{josepcarol_web_volume_path}}
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - {{josepcarol_node}}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: "{{josepcarol_web_volume}}-pvc"
  namespace: {{josepcarol_kube_ns}}
spec:
  storageClassName: local-storage
  volumeName: "{{josepcarol_web_volume}}-pv"
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
---
apiVersion: v1
kind: Service
metadata:
  name: {{josepcarol_service}}
  namespace: {{josepcarol_kube_ns}}
spec:
  ports:
  - name: web
    port: 80
    targetPort: 80
  selector:
    app: {{josepcarol_service}}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{josepcarol_service}}
  namespace: {{josepcarol_kube_ns}}
  labels:
    app: {{josepcarol_service}}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{josepcarol_service}}
  template:
    metadata:
      labels:
        app: {{josepcarol_service}}
    spec:
      nodeName: {{josepcarol_node}}
      containers:
      - image: {{josepcarol_nginx_image}}
        name: "{{josepcarol_service}}-nginx"
        ports:
        - containerPort: 80
          name: http
        volumeMounts:
        - name: josepcarol-nginx-conf
          mountPath: /etc/nginx/conf.d/default.conf
          subPath: default.conf
        - name: "{{josepcarol_web_volume}}"
          mountPath: /var/www/html
      - image: {{josepcarol_web_image}}
        name: "{{josepcarol_service}}-web"
        ports:
        - containerPort: 9000
          name: fpm
        volumeMounts:
        - name: "{{josepcarol_web_volume}}"
          mountPath: /var/www/html
      volumes:
      - name: josepcarol-nginx-conf
        configMap:
          name: josepcarol-nginx-conf
      - name: "{{josepcarol_web_volume}}"
        persistentVolumeClaim:
          claimName: "{{josepcarol_web_volume}}-pvc"
      dnsPolicy: None
      dnsConfig:
        nameservers:
          - {{kube_master_node}}
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: {{josepcarol_service}}
  namespace: {{josepcarol_kube_ns}}
spec:
  rules:
  - host: "{{josepcarol_domain}}"
    http:
      paths:
      - path: /
        backend:
          serviceName: {{josepcarol_service}}
          servicePort: web
