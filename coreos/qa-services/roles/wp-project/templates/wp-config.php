<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', '{{wp_project_db_name}}' );

/** MySQL database username */
define( 'DB_USER', '{{wp_project_service}}' );

/** MySQL database password */
define( 'DB_PASSWORD', '{{wp_project_db_password}}' );

/** MySQL hostname */
define( 'DB_HOST', '{{mariadb_front_host}}' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'XaQcP><%}dA RHH*#FFl5oi6EusZzLE^epiJU@,.1i=K0-O@-CqwEep|mVe3uckM' );
define( 'SECURE_AUTH_KEY',  'ub8A=Pk@h|QR`tU^9y`,Ea:~[KJ^1 !mNk:(:iX?107W$3$TF tFlqde~x@+!ps]' );
define( 'LOGGED_IN_KEY',    '!ktF{i}a(f 3[rC#{f!Z93H=LhuPL _m%|N9?BFi[Z:kK:u[2:L^&Lg9,yasifV}' );
define( 'NONCE_KEY',        ']d>8ZV0G=rI:I+^V))]=x.VTb@/^ejxc{j;uYP7vB`:;WPSNvT0q#iCgB>~$@Kjh' );
define( 'AUTH_SALT',        'h p%G9#=,V.5:5_B~n`^~Uob>(MV(.UI8JmLhV}uYn7TpPW|u9,694JR4x5)<k@J' );
define( 'SECURE_AUTH_SALT', 'Xrg1:}],[^(XqH*2navDl^~6#+TF*ve2j*C`XD`TH{4(iIUufWCZL;#lV$<H20$-' );
define( 'LOGGED_IN_SALT',   '9;[[83|lM,6)g2B=^9qjs_QyahQURMKc3i#``o7r;:ZEA2Fb_?6xpJuLxB,L>kVQ' );
define( 'NONCE_SALT',       'o6MiFAd{w32pXb&gFMz%Y(f6[t0T;k#yPzS9[6Ht54?5End0 #E/TZ=+KNO7g`ox' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

// If we're behind a proxy server and using HTTPS, we need to alert Wordpress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );
define('WP_MEMORY_LIMIT', '256M');

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', true );
define('FS_METHOD', 'direct');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

