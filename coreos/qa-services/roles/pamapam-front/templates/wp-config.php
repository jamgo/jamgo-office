<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '{{pamapam_front_db_name}}');

/** MySQL database username */
define('DB_USER', '{{pamapam_front_db_user}}');

/** MySQL database password */
define('DB_PASSWORD', '{{pamapam_front_db_password}}');

/** MySQL hostname */
define('DB_HOST', '{{mariadb_front_host}}');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b605d163f34b1e635edcb8c6744eb61af303d6c2');
define('SECURE_AUTH_KEY',  '65f2a93fb137b7665d745b9a739a37d92890adb7');
define('LOGGED_IN_KEY',    '196de050cfa575e47e3e0bc6ee3cc674cf85acc8');
define('NONCE_KEY',        '54c6fa772c6c95b7a64d4b341adb43d4a0789957');
define('AUTH_SALT',        '21ebdc9fb3970af7e27f2bb9d71cdcd3a3f3f181');
define('SECURE_AUTH_SALT', 'b536a3e71d2b15ef4eda71834d5b671dc5a358e0');
define('LOGGED_IN_SALT',   'ec2330a22380f8faf82d3c944c0202e1987c3be7');
define('NONCE_SALT',       '8c53d48d46c1b282e9c18ed0e5dcfa2fb3de5e0b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

// If we're behind a proxy server and using HTTPS, we need to alert Wordpress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Backoffice connection urls. */
define('BASE_API_URL', 'https://pamapam.jamgo.org/services');
define('BASE_API_INTERNAL_URL', 'https://pamapam.jamgo.org/services');
define('BACKOFFICE_URL', 'https://pamapam.jamgo.org/backoffice/ui');
	
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
	
define('WP_MEMORY_LIMIT', '256M');
	