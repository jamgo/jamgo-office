#!/bin/bash

HARDWARE_TYPE=""
SERVICE=""

function usage() {
    printf "\n"
    printf "Uninstall kubernetes service\n"
    printf "\n"
    printf "Usage:\n"
    printf "\tuninstall.sh (-t|--type) (bare-metal|qemu) <service_yml>\n"
    printf "\tuninstall.sh -h|--help\n"
    printf "\n"
    printf "Options:\n"
    printf "\tbare-metal, qemu\tHardware type.\n"
    printf "\n"
}

while [ "$1" != "" ]; do
#    PARAM=`echo $1 | awk -F= '{print $1}'`
#    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $1 in
        -h | --help)
            usage
            exit
            ;;
        -t | --type)
        	if [ -n "$HARDWARE_TYPE" ]; then
        		printf "ERROR: Only one hardware type!\n"
        		usage
        		exit 1
        	fi
        	shift
        	case $1 in
        		bare-metal | qemu)
            		HARDWARE_TYPE=$1
            		;;
            	*)
        			printf "ERROR: Unknown hardware type\n"
        			printf "$1"
        			exit 1
        			;;
        	esac
        	;;
        *)
        	if [ -n "$SERVICE" ]; then
        		printf "ERROR: Only one service at a time!\n"
        		usage
        		exit 1
        	fi
            SERVICE=$1
            ;;
    esac
    shift
done

if [ -z $HARDWARE_TYPE ]; then
	printf "ERROR: Must select a hardware type.\n"
	usage
	exit 1
fi

if [ -z $SERVICE ]; then
	printf "ERROR: Must select a service.\n"
	usage
	exit 1
fi

STATE_PREFIX=$(echo -n "${SERVICE}" | sed -e 's/\.yml//g' -e 's/-/_/g')
ansible-playbook -i ../ansible-common/inventories/$HARDWARE_TYPE/ $SERVICE -e "${STATE_PREFIX}_state=absent"
