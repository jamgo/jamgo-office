#!/bin/bash

SERVICE=""
DEBUG=""
VERBOSE=""
ACTION=""

function usage() {
    printf "\n"
    printf "Installs in kubernetes cluster\n"
    printf "\n"
    printf "Usage:\n"
    printf "\tinstall.sh <service.yaml> (-a|--action) <action> [-d|--debug] [-v|--verbose]\n"
    printf "\tinstall.sh -h|--help\n"
    printf "\n"
    printf "Options:\n"
    printf "\t-d/--debug\tExecute onsible step by step.\n"
    printf "\t-v/--verbose\tAnsible output verbose.\n"
    printf "\n"
}

while [ $# -ge 1 ]; do
    case $1 in
        -h | --help)
            usage
            exit
            ;;
        -d | --debug)
            DEBUG="--step"
            shift
            ;;
        -v | --verbose)
            VERBOSE="-vvv"
            shift
            ;;
        -a | --action)
            ACTION="$2"
            shift 2
            ;;
        *)
        	if [ -n "$SERVICE" ]; then
        		printf "ERROR: Only one service at a time!\n"
        		usage
        		exit 1
        	fi
            SERVICE=$1
            shift
            ;;
    esac
done

if [ -z $SERVICE ]; then
	printf "ERROR: Must select a service.\n"
	usage
	exit 1
fi

ansible-playbook $DEBUG $VERBOSE -i hosts $SERVICE -e "action=$ACTION"
