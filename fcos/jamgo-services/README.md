## Playbooks

* *jamgo-cluster.yml*: Update full cluster.
* *<service-name>-service.yml*: Update single service.
* *bootstrap.yml*: Install basic packages in CoreOS to allow ansible run.
* *dump.yml*: Dumps all variables in /tmp/ansible_dump.all on each host.
* *upgrade-kubernetes*: Downldoas and update kubernetes using versions defined in group_vars/all

## Add or remove service

```bash
ansible-playbook -i <inventory> <playbook> --extra-vars "<service>_state=[present|absent]"
```

### Ex. remove wiki service on qemu cluster

```bash
ansible-playbook -i inventories/qemu jamgo-cluster.yml --extra-vars "wiki_state=absent"
```

## Encrypt vault string

```bash
 echo -n "<the string>" | ansible-vault encrypt_string --stdin-name '<variable name>'
```
## Open Dashboard

```bash
kubectl -n kube-system get secret
kubectl -n kube-system describe secret deployment-controller-token-xxxx
https://dashboard.<office-domain>
```