---
kind: ConfigMap
apiVersion: v1
metadata:
  name: wikijs-config-yml
  namespace: "{{wikijs_kube_ns}}"
data:
  config.yml: |
    #######################################################################
    # Wiki.js - CONFIGURATION                                             #
    #######################################################################
    # Full documentation + examples:
    # https://docs.requarks.io/install
    
    # ---------------------------------------------------------------------
    # Port the server should listen to
    # ---------------------------------------------------------------------
    
    port: 3000
    
    # ---------------------------------------------------------------------
    # Database
    # ---------------------------------------------------------------------
    # Supported Database Engines:
    # - postgres = PostgreSQL 9.5 or later
    # - mysql = MySQL 8.0 or later (5.7.8 partially supported, refer to docs)
    # - mariadb = MariaDB 10.2.7 or later
    # - mssql = MS SQL Server 2012 or later
    # - sqlite = SQLite 3.9 or later
    
    db:
      type: postgres
    
      # PostgreSQL / MySQL / MariaDB / MS SQL Server only:
      host: {{postgres_office_host}}
      port: {{postgres_office_port}}
      user: {{wikijs_db_user}}
      pass: {{wikijs_db_password}}
      db: {{wikijs_db_name}}
      ssl: false
    
      # Optional - PostgreSQL / MySQL / MariaDB only:
      # -> Uncomment lines you need below and set `auto` to false
      # -> Full list of accepted options: https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options
      sslOptions:
        auto: true
        # rejectUnauthorized: false
        # ca: path/to/ca.crt
        # cert: path/to/cert.crt
        # key: path/to/key.pem
        # pfx: path/to/cert.pfx
        # passphrase: xyz123
    
      # SQLite only:
      # storage: /config/db.sqlite
    
    #######################################################################
    # ADVANCED OPTIONS                                                    #
    #######################################################################
    # Do not change unless you know what you are doing!
    
    # ---------------------------------------------------------------------
    # SSL/TLS Settings
    # ---------------------------------------------------------------------
    # Consider using a reverse proxy (e.g. nginx) if you require more
    # advanced options than those provided below.
    
    ssl:
      enabled: false
      port: 3443
    
      # Provider to use, possible values: custom, letsencrypt
      provider: custom
    
      # ++++++ For custom only ++++++
      # Certificate format, either 'pem' or 'pfx':
      format: pem
      # Using PEM format:
      key: path/to/key.pem
      cert: path/to/cert.pem
      # Using PFX format:
      pfx: path/to/cert.pfx
      # Passphrase when using encrypted PEM / PFX keys (default: null):
      passphrase: null
      # Diffie Hellman parameters, with key length being greater or equal
      # to 1024 bits (default: null):
      dhparam: null
    
      # ++++++ For letsencrypt only ++++++
      domain: wiki.yourdomain.com
      subscriberEmail: admin@example.com
    
    # ---------------------------------------------------------------------
    # Database Pool Options
    # ---------------------------------------------------------------------
    # Refer to https://github.com/vincit/tarn.js for all possible options
    
    pool:
    # min: 2
    # max: 10
    
    # ---------------------------------------------------------------------
    # IP address the server should listen to
    # ---------------------------------------------------------------------
    # Leave 0.0.0.0 for all interfaces
    
    bindIP: 0.0.0.0
    
    # ---------------------------------------------------------------------
    # Log Level
    # ---------------------------------------------------------------------
    # Possible values: error, warn, info (default), verbose, debug, silly
    
    logLevel: info
    
    # ---------------------------------------------------------------------
    # Upload Limits
    # ---------------------------------------------------------------------
    # If you're using a reverse-proxy in front of Wiki.js, you must also
    # change your proxy upload limits!
    
    uploads:
      # Maximum upload size in bytes per file (default: 5242880 (5 MB))
      maxFileSize: 104857600
      # Maximum file uploads per request (default: 10)
      maxFiles: 10
    
    # ---------------------------------------------------------------------
    # Offline Mode
    # ---------------------------------------------------------------------
    # If your server cannot access the internet. Set to true and manually
    # download the offline files for sideloading.
    
    offline: false
    
    # ---------------------------------------------------------------------
    # High-Availability
    # ---------------------------------------------------------------------
    # Set to true if you have multiple concurrent instances running off the
    # same DB (e.g. Kubernetes pods / load balanced instances). Leave false
    # otherwise. You MUST be using PostgreSQL to use this feature.
    
    ha: false
    
    # ---------------------------------------------------------------------
    # Data Path
    # ---------------------------------------------------------------------
    # Writeable data path used for cache and temporary user uploads.
    dataPath: /data
---
kind: PersistentVolume
apiVersion: v1
metadata:
  name: "{{wikijs_config_volume}}-pv"
spec:
  storageClassName: local-storage
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  local:
    path: "{{wikijs_config_volume_path}}"
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - "{{wikijs_node}}"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: "{{wikijs_config_volume}}-pvc"
  namespace: "{{wikijs_kube_ns}}"
spec:
  storageClassName: local-storage
  volumeName: "{{wikijs_config_volume}}-pv"
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
---
kind: PersistentVolume
apiVersion: v1
metadata:
  name: "{{wikijs_data_volume}}-pv"
spec:
  storageClassName: local-storage
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  local:
    path: "{{wikijs_data_volume_path}}"
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - "{{wikijs_node}}"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: "{{wikijs_data_volume}}-pvc"
  namespace: "{{wikijs_kube_ns}}"
spec:
  storageClassName: local-storage
  volumeName: "{{wikijs_data_volume}}-pv"
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: "{{wikijs_service}}"
  namespace: "{{wikijs_kube_ns}}"
  labels:
    app: "{{wikijs_service}}"
spec:
  replicas: 1
  selector:
    matchLabels:
      app: "{{wikijs_service}}"
  template:
    metadata:
      labels:
        app: "{{wikijs_service}}"
    spec:
      nodeName: "{{wikijs_node}}"
      containers:
      - image: "{{wikijs_image}}"
        name: "{{wikijs_service}}"
        ports:
        - containerPort: 3000
          name: wikijs
        volumeMounts:
        - name: "{{wikijs_config_volume}}"
          mountPath: /config/config.yml
          subPath: config.yml
        - name: "{{wikijs_data_volume}}"
          mountPath: /data
      volumes:
      - name: "{{wikijs_config_volume}}"
        configMap:
          name: wikijs-config-yml
      - name: "{{wikijs_data_volume}}"
        persistentVolumeClaim:
          claimName: "{{wikijs_data_volume}}-pvc"
      dnsPolicy: None
      dnsConfig:
        nameservers:
          - "{{kube_master_node}}"
---
apiVersion: v1
kind: Service
metadata:
  name: "{{wikijs_service}}"
  namespace: "{{wikijs_kube_ns}}"
spec:
  ports:
  - name: web
    port: 80
    targetPort: 3000
  selector:
    app: "{{wikijs_service}}"
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: "{{wikijs_service}}"
  namespace: "{{wikijs_kube_ns}}"
  annotations:
    kubernetes.io/ingress.class: traefik-internal
spec:
  rules:
  - host: "{{wikijs_service}}.{{office_domain}}"
    http:
      paths:
      - pathType: Prefix
        path: /
        backend:
          service:
            name: "{{wikijs_service}}"
            port:
              number: 80
