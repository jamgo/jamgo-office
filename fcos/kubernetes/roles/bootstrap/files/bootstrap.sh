#/bin/bash

set -e

cd

if [[ -e $HOME/.bootstrapped ]]; then
  exit 0
fi

sudo rpm-ostree install python python3-kubernetes --idempotent

touch $HOME/.bootstrapped
