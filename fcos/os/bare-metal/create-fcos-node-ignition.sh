#!/bin/bash

while [ $# -ge 1 ]; do
    case "$1" in
        -h|--hostname)
            HOSTNAME="$2"
            shift 2 ;;
        -a|--mac-address)
            MAC_ADDRESS="$2"
            shift 2 ;;
        -i|--ip)
            IP="$2"
            shift 2 ;;
        -g|--gateway)
            GATEWAY="$2"
            shift 2 ;;
        -d|--dns)
            DNS="$2"
            shift 2 ;;
        --)
            shift
            break ;;
        *)
            break ;;
    esac
done

source ./env

cp ./fcos-node-template.yml "${CONFIG_FILE}"
sed -i "s|{{hostname}}|${HOSTNAME}|" "${CONFIG_FILE}"
sed -i "s|{{ssh_key}}|${SSH_KEY}|" "${CONFIG_FILE}"
sed -i "s|{{mac_address}}|${MAC_ADDRESS}|" "${CONFIG_FILE}"
sed -i "s|{{ip}}|${IP}|" "${CONFIG_FILE}"
sed -i "s|{{gateway}}|${GATEWAY}|" "${CONFIG_FILE}"
sed -i "s|{{dns}}|${DNS}|" "${CONFIG_FILE}"

docker run -i --rm quay.io/coreos/butane:release --pretty --strict < "${CONFIG_FILE}" > "${IGNITION_FILE}"
