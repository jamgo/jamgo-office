#!/bin/bash

source ./env

./create-fcos-node-ignition.sh -h wilma -i 10.7.13.10/24 -g 10.7.13.1 -d 8.8.8.8
./create-fcos-node-ignition.sh -h betty -i 10.7.13.11/24 -g 10.7.13.1 -d 8.8.8.8
