#!/bin/bash

VM_NAME=$1
IP=$2 

ssh-keygen -f ~/.ssh/known_hosts -R "${IP}"

echo "Starting..."
sudo virsh start "${VM_NAME}"

while ! nc -z "${IP}" 22; do   
	sleep 1
done

ssh-keyscan -H "${IP}" >> ~/.ssh/known_hosts
