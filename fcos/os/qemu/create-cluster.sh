#!/bin/bash

source ./env

echo "${IMAGE_NAME}"
echo "${BASE_IMAGE_FILE}"

if [ ! -f "${BASE_IMAGE_FILE}" ]; then
	wget https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/${FCOS_VERSION}/x86_64/${IMAGE_NAME}.xz -O ${BASE_IMAGE_FILE}.xz 
	echo "Decompressing Fedora CoreOS image file..."
	unxz -d "${BASE_IMAGE_FILE}".xz
fi

./create-fedora-coreos-vm.sh -h wilma -m 16384 -c 6 -s 200 -i 10.19.0.2/16 -g 10.19.0.1 -d 10.19.0.1 -a 52:54:00:fe:b3:c0
./create-fedora-coreos-vm.sh -h betty -m 16384 -c 6 -s 200 -i 10.19.0.3/16 -g 10.19.0.1 -d 10.19.0.1 -a 52:54:00:fe:b3:c1
