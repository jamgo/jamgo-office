#!/bin/bash

source ./env
VM_NAME=$1

purge() {
	echo "Purging..."
	sudo virsh shutdown "${VM_NAME}"
	sudo virsh undefine "${VM_NAME}"
	sudo rm "${IMAGES_PATH}"/"${VM_NAME}.qcow2"
	exit
}

while true
do
	read -r -p "Purge ${VM_NAME} will remove also disk images. Are you Sure? [yes/no] " input

	case $input in
		[yY][eE][sS])
		purge
		;;

		[nN][oO])
		echo "Bye"
		exit
		;;

		*)
		echo "Pleas enter 'yes' or 'no'"
		;;
	esac
done