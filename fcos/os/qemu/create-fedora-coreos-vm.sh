#!/bin/bash

while [ $# -ge 1 ]; do
    case "$1" in
        -h|--hostname)
            HOSTNAME="$2"
            shift 2 ;;
        -a|--mac-address)
            MAC_ADDRESS="$2"
            shift 2 ;;
        -m|--memory)
            MEMORY="$2"
            shift 2 ;;
        -c|--cpus)
            CPUS="$2"
            shift 2 ;;
        -i|--ip)
            IP="$2"
            shift 2 ;;
        -g|--gateway)
            GATEWAY="$2"
            shift 2 ;;
        -d|--dns)
            DNS="$2"
            shift 2 ;;
        -s|--storage-size)
            STORAGE_SIZE="$2"
            shift 2 ;;
        --)
            shift
            break ;;
        *)
            break ;;
    esac
done

source ./env

# qemu-img create -f qcow2 -b "${IMAGES_PATH}"/coreos_production_qemu_image.img "${IMAGE_FILE}" 200G

cp ./fcos-node-template.yml "${CONFIG_FILE}"
sed -i "s|{{hostname}}|${HOSTNAME}|" "${CONFIG_FILE}"
sed -i "s|{{ssh_key}}|${SSH_KEY}|" "${CONFIG_FILE}"
sed -i "s|{{mac_address}}|${MAC_ADDRESS}|" "${CONFIG_FILE}"
sed -i "s|{{ip}}|${IP}|" "${CONFIG_FILE}"
sed -i "s|{{gateway}}|${GATEWAY}|" "${CONFIG_FILE}"
sed -i "s|{{dns}}|${DNS}|" "${CONFIG_FILE}"
docker run -i --rm quay.io/coreos/butane:release --pretty --strict < "${CONFIG_FILE}" > "${IGNITION_FILE}"

sudo virt-install \
 --name=${HOSTNAME} \
 --vcpus=${CPUS} \
 --ram="${MEMORY}" \
 --os-variant=fedora-coreos-stable \
 --import \
 --network=network=nat,model=virtio,mac="${MAC_ADDRESS}" \
 --check=mac_in_use=off \
 --graphics=none \
 --noautoconsole \
 --noreboot \
 --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=${PWD}/${IGNITION_FILE}" \
 --disk=path="${IMAGE_FILE}",format=qcow2,size=${STORAGE_SIZE},bus=virtio,backing_store="${BASE_IMAGE_FILE}"
# --disk=path="${IMAGE_FILE}",format=qcow2,size=${STORAGE_SIZE},bus=virtio,backing_store="${BASE_IMAGE_FILE}" \
# --filesystem=/data/${HOSTNAME}-qemu/srv/volumes/,${HOSTNAME}-qemu-volumes,driver.type=virtiofs

ssh-keygen -f ~/.ssh/known_hosts -R ${IP%/*}
